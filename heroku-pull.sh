dropdb zapaf-ug_development
heroku pg:pull DATABASE_URL zapaf-ug_development --app zapaf-ug

# now that im pulling the current state of the db from heroku
# prob should just export from local db

# heroku pg:psql --app zapaf-ug DATABASE -c '\COPY notes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/notes.csv
# heroku pg:psql --app zapaf-ug DATABASE -c '\COPY areas TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/areas.csv
# heroku pg:psql --app zapaf-ug DATABASE -c '\COPY locations TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/locations.csv
# heroku pg:psql --app zapaf-ug DATABASE -c '\COPY relationships TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/relationships.csv
# heroku pg:psql --app zapaf-ug DATABASE -c '\COPY areas_locations TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/areas_locations.csv
# heroku pg:psql --app zapaf-ug DATABASE -c '\COPY locations_notes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/locations_notes.csv

psql zapaf-ug_development -c '\COPY notes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/notes.csv
psql zapaf-ug_development -c '\COPY areas TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/areas.csv
psql zapaf-ug_development -c '\COPY locations TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/locations.csv
psql zapaf-ug_development -c '\COPY relationships TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/relationships.csv
psql zapaf-ug_development -c '\COPY areas_locations TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/areas_locations.csv
psql zapaf-ug_development -c '\COPY locations_notes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/locations_notes.csv


