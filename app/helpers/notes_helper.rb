module NotesHelper
  # for local ip
  require 'socket'
  def show_area_datetime(location)
    d_x = location.area_datetime.date_x
    d_x = d_x.strftime('%b %-d, %Y') if d_x
    t_x = location.area_datetime.time_x
    t_x = t_x.strftime('%l:%M %P') if t_x
    d_y = location.area_datetime.date_y
    d_y = d_y.strftime('%b %-d, %Y') if d_y
    t_y = location.area_datetime.time_y
    t_y = t_y.strftime('%l:%M %P') if t_y
    "#{d_x} #{t_x} - #{d_y} #{t_y}"
  end

  def openstreet_src(loc)
    p = [lox(loc).to_s, lax(loc).to_s, loy(loc).to_s, lay(loc).to_s].join('%2C')
    "https://www.openstreetmap.org/export/embed.html?bbox=#{p}&layer=mapnik"
  end

  def openstreet_href(loc)
    # "https://www.openstreetmap.org/#map=12/51.5370/9.8839&amp;layers=ND"
    m = openstreet_map_number((lay(loc) - lax(loc)))
    # lax = location.area_latitude.x
    # lay = location.area_latitude.y
    la = (lay(loc) + lax(loc)) / 2
    lo = (loy(loc) + lox(loc)) / 2
    "https://www.openstreetmap.org/#map=#{m.to_i}/#{la}/#{lo}&amp;layers=ND"
  end

  def lax(loc)
    loc.area_latitude.x
  end

  def lay(loc)
    loc.area_latitude.y
  end

  def lox(loc)
    loc.area_longitude.x
  end

  def loy(loc)
    lo = loc.area_longitude.y
    lo = loc.area_longitude.y + 360 if loc.area_longitude.y < loc.area_longitude.x
    lo
  end

  def openstreet_map_number(la_size)
    case la_size * 10
    when 0..5
      12
    when 5..9
      9
    when 9..10
      8
    when 10..50
      7
    when 50..100
      6
    when 100..150
      5
    when 150..300
      4
    else
      3
    end
  end

  def local_machine?
    remote_ip == '127.0.0.1'
  end

  def remote_ip
    # Net::HTTP.get(URI.parse('http://checkip.amazonaws.com/')).squish
    request.remote_ip # 127.0.0.1
  end

  def local_ip
    # 172.20.164.82
    # turn off reverse DNS resolution temporarily
    orig = Socket.do_not_reverse_lookup
    Socket.do_not_reverse_lookup = true

    UDPSocket.open do |s|
      s.connect '64.233.187.99', 1
      s.addr.last
    end
  ensure
    Socket.do_not_reverse_lookup = orig
  end
end
