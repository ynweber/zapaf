# frozen_string_literal: true

# the relationships controller
class RelationshipsController < ApplicationController
  before_action :set_rel
  def att_note
    note = Note.create(name: "#{@rel.parent.name} & #{@rel.child.name}")
    @rel.note_id = note.id
    @rel.save
    redirect_back(fallback_location: :root)
  end

  private

  def set_rel
    @rel = Relationship.find(params[:id])
  end
end
