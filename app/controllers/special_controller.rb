# frozen_string_literal: true

# the notes controller
class SpecialController < ApplicationController
  def spreadsheet_import
    if params[:spreadsheet] && params[:spreadsheet][:f_name]
      redirect_to controller: 'special', action: 'spreadsheet_warning', f_name: params[:spreadsheet][:f_name]
    end
  end

  def spreadsheet_warning
    @note = Special.spreadsheet_import(params[:f_name])
    @note.rebuild_relatives
    render('notes/show', layout: 'spreadsheet_import')
  end
end
