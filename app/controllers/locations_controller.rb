# frozen_string_literal: true

# the locations controller
class LocationsController < ApplicationController
  def show
    @q = Note.ransack(params[:q])
    @note = Note.find(28)
    search = params[:search].delete_if { |_k, v| v.empty? }
    @note.children = Note.by_location(search)
    # @note.children = Note.by_location(
    #   search[:latitude], search[:longitude],
    #   search[:elevation],
    #   search[:date],
    #   search[:time]
    # )
    redirect_to notes_path(@note)
  end

  def search; end
end
