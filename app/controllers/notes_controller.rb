# frozen_string_literal: true

# the notes controller
class NotesController < ApplicationController
  before_action :set_note, :check_note, :set_sortfield, except: :exec_int

  def index; end

  def show
    # relatives builds instance variable based on sortfield
    # @note.relatives(@sortfield)
  end

  def read
    @note.relatives('rel_name')
    render layout: 'read'
  end

  def chain
    @note.chain
    render 'show'
  end

  def add_birth_and_death
    birth = Relationship.new
    birth.name = 'birth'
    birth_note = Note.new(text: '')
    birth_note.name = "birth of #{@note.name}}"
    birth_note.save
    birth.parent_id = @note.id
    birth.child_id = birth_note.id
    birth.save
    death_note = Note.new(text: '')
    death_note.name = "death of #{@note.name}}"
    death_note.save
    Relationship.create(name: 'death', parent_id: @note.id, child_id: death_note.id)
    redirect_back(fallback_location: :root)
  end

  def refresh
    @note.refresh
    Tbc.append(@note.id)
    Tbc.full_check
    # (1..100).each do |cnt|
    #   break if Tbc.list.empty?
    #   Tbc.check_tbc(cnt)
    # end
    puts 'Still Remaining: ' + Tbc.list.length.to_s.red
    puts 'COMPLETE'.light_green if Tbc.list.empty?
    redirect_back(fallback_location: :root)
  end

  def refresh_quick
    @note.refresh_magic
    redirect_back(fallback_location: :root)
  end

  def exec_int
    fork { system("libreoffice -o public/int_files/#{params[:int_file]}") }
    redirect_back(fallback_location: :root)
  end

  private

  def set_sortfield
    @sortfield = params[:sortfield] || 'rel_name'
  end

  def set_note
    if params[:q]
      params[:q]['name_cont_all'] = params[:q]['name_cont_all'].split(' ')
      params[:q]['text_cont_all'] = params[:q]['text_cont_all'].split(' ')
    end

    if params[:nikud_search]
      @note = Note.new
      @note.name = 'search results'
      children = Note.where("regexp_replace(text, '[\u0591-\u05c7]', '', 'g') ilike '%#{params[:nikud_search]['word']}%'")
      children = children.where("name ilike '%#{params[:nikud_search][:name]}%'")
      @note.children = children
    end

    @q = Note.ransack(params[:q]) # if params[:q]
    @note ||= if params[:q]
              search_results
            elsif params[:id].to_i > 0
              Note.find(params[:id])
            else
              Note.find(66)
            end
  end

  def search_results
    @search_results = @q.result # .includes(:locations)
    note = Note.new
    note.name = 'Search Results'
    note.relatives_unsaved(@search_results)
    note
  end

  # this needs to be done on mode, but being thorough
  def check_note
    return if user_signed_in?
    if @note.is_private
      redirect_to :root
    else
      # it kept writing to db
      note = Note.new
      note.attributes = @note.attributes
      note.children = @note.children.to_a.delete_if(&:is_private)
      note.parents = @note.parents.to_a.delete_if(&:is_private)
      @note = note
    end
  end
end
