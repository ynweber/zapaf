# frozen_string_literal: true

class SpecialImport < ApplicationRecord
  def self.spreadsheet_import(f_name)
    require 'csv'
    fi = Note.find_or_create_by(name: 'file_import')
    fi.child_relationships.each(&:delete)
    CSV.foreach("public/int_files/#{f_name}", headers: true) do |row|
      @th ||= row.map(&:first)
      notes = row.map do |cell|
        next unless cell[1]
        f_or_i(fi.id, cell[1])
      end
      notes = notes.compact

      notes.each_with_index do |note, col_i|
        next if col_i.zero?
        relate_note_with_row(note, @th[col_i], notes)
      end
    end
    fi
  end

  def self.f_or_i(fi_id, cell)
    return unless cell.present? && cell.length > 1
    cell = cell.gsub("\u2013", '-')
    # return if @cell_list.include?(cell)
    # @cell_list.append(cell)
    r_a = Note.find_or_initialize_by(name: cell)
    r_a_name = 'old'
    if r_a.new_record?
      r_a_name = 'new'
      r_a.text = ''
      r_a.save
    end
    r_a_r = Relationship.find_or_initialize_by(
      parent_id: fi_id, child_id: r_a.id
    )
    r_a_r.name = r_a_name
    r_a_r.save unless r_a_r.id.present?
    r_a
  end

  def self.relate_note_with_row(note, col_name, notes)
    return unless col_name.present? && col_name.length.positive?
    notes.each do |r_note|
      puts r_note
      rel = Relationship.find_or_create_by(
        parent_id: note.id,
        child_id: r_note.id
      )
      rel.name = col_name
      rel.save
    end
  end
end

