# frozen_string_literal: true

# !! for later refactoring !!
# keep in mind it may be better (better than relatives build)
# to start at the join model Relationship
#
# validator
class NoteValidator < ActiveModel::Validator
  def validate(note)
    deps = note.depends.full
    note.errors.add :base, 'self reference' if deps.include?(note.id)
  end
end

# the note model
class Note < ApplicationRecord
  # located in app/model/concerns
  include NoteAdmin
  has_many :relationship_notes, class_name: 'Relationship', foreign_key: :note_id
  has_many :parent_relationships,
           class_name: 'Relationship', foreign_key: :child_id,
           autosave: false
  has_many :child_relationships,
           class_name: 'Relationship', foreign_key: :parent_id,
           autosave: false
  has_many :children,
           through: :child_relationships,
           foreign_key: :parent_id, autosave: false
  has_many :parents,
           through: :parent_relationships, foreign_key: :child_id,
           autosave: false
  has_and_belongs_to_many :locations, autosave: false

  validates_with NoteValidator
  # attr_accessor :relationship
  # attr_accessor :flash_notice

  # # called by Relationship and controller
  # def self.tbc(note_id = nil)
  #   Tbc.append(note_id)
  # end

  def tags
    puts 'tags'
    ts = self[:tags]
    # this needs to be somewhere else!!
    # ts.push('tag') if child_relationships.length > 100
    puts 'done tags'
    ts
  end

  def self.tag_tags
    p_ids = Relationship.group(:parent_id)
                        .having('count(parent_id) > 100').select(:parent_id)
                        .map(&:parent_id)

    new_tag_notes = where("'tag' != all(tags)").where(id: p_ids)
    new_tag_notes.each do |n|
      n.tags += ['tag']
      n.save
    end

    old_tag_notes = where("'tag' = any(tags)").where.not(id: p_ids)
    old_tag_notes.each do |n|
      n.tags -= ['tag']
      n.save
    end
    p_ids
  end

  def date
    return locations[0].area_datetime.date_x if locations.any?
    created_at
  end

  def relatives(sf_name = nil)
    @relatives || rebuild_relatives(sf_name)
  end

  # made for post spreadsheet import
  # sort by sort field
  def rebuild_relatives(sf_name = nil)
    puts 'rebuild_relatives'
    @relatives = rel_build.uniq
    @relatives = @relatives.sort_by { |rel| rel.sort_field(sf_name) || '' } if sf_name
    @relatives
    
    # @relatives = rel_build.uniq.sort_by { |rel| rel.child.send(stf) }
  end

  # made for search
  def relatives_unsaved(rels)
    @relatives = []
    rels[0..300].each do |rel|
      new_r = Relationship.new(
        parent_id: id,
        child_id: rel.id
      )
      @relatives.push(new_r)
    end
  end

  def rel_build
    puts 'rel_build'
    rs = child_relationships.reload
    puts 'rel_build parent'
    parent_rs = parent_relationships.reload
    # i can put flip back, not the prob
    rs += parent_rs.each { |pr| pr[:child_id] = pr[:parent_id] } # (&:flip)
    puts 'done rebuild'
    rs
  end

  def rev_magic_pos
    puts 'rev_magic_pos'
    ns = Note.where('array[or_ids] && array[(?)]', [id])
    ns = ns.or(Note.where('array[and_ids] && array[(?)]', [id]))
    ns = ns.or(Note.where('array[relative_ids] && array[(?)]', [id]))
    ns
  end

  def self.pg_array(ary)
    ary.to_s.tr('[]"', '')
  end

  def self.pg_array_i(ary)
    ary
    # ary.to_s.tr('[]"\'', '')
  end

  def self.rev_magic_group_pos(notes)
    puts 'rev_magic_group_pos'
    return [] unless notes.any?
    # n_ids = Note.pg_array_i(notes.map(&:id))
    n_ids = notes.map(&:id)
    ns = Note.where('array[or_ids] && array[?]', n_ids)
    ns = ns.or(Note.where('array[and_ids] && array[?]', n_ids))
    ns = ns.or(Note.where('array[relative_ids] && array[?]', n_ids))
    ns = ns.where("'tag' != all(tags)")
    ns
  end

  def self.rev_magic_group(notes)
    puts 'rev_magic_group'
    ns = rev_magic_group_pos(notes)
    n_ids = notes.map(&:id)
    ns = ns.or(Note.where('array[not_ids] && array[?]', n_ids))
    ts = Note.pg_array(notes.map(&:tags).flatten.compact.uniq)
    ns = ns.or(Note.where('array[(?)] && array[(filter_tags)]', ts))
    ns
  end

  def rev_magic
    ns = rev_magic_pos
    ns = ns.or(Note.where('array[not_ids] && array[(?)]', [id]))
    ns = ns.or(
      Note.where('array[(?)] && array[(filter_tags)]', tags.to_s.tr('[]"', ''))
    )
    ns
  end

  def short_refresh
    puts 'short_refresh'
    Note.find(depends.all).each(&:refresh_magic)
    Note.refresh_magic
  end

  def magical?
    depends.all.any? ||
      self[:having_count].present? ||
      self[:magic].present? ||
      self[:filter_tags].any?
  end

  def depends
    Depends.new(self)
  end

  def refresh
    # i = refresh_magic
    # while c < 10 && i > 0
    Abc.append(id)
    ns = Note.where(id: depends.full)
    ns += rev_magic
    ns.uniq.each(&:refresh_magic)
    refresh_magic
  end

  def refresh_magic
    puts "RUNNING MAGIC #{id} #{name}"
    return 0 if depends.all.empty? && self[:filter_rname].empty?
    rels = Rels.new(self)
    rels.update_relatives
    # update_relatives(rels)
  end

  def img
    self[:img] || ''
  end

  def img_small
    self[:img32] || img
  end

  def ext_link_name
    self[:ext_link_name]
  end
end

# which notes a magic note "depends" on fro relationships.
class Depends
  def initialize(note)
    @note = note
    @deps_full = []
  end

  def all
    puts 'Depends.all'
    (pos + @note.not_ids).uniq
  end

  def pos
    puts 'Depends.pos'
    (@note.or_ids + @note.and_ids + @note.relative_ids).uniq
  end

  def depends_deeper
    puts 'depends_deeper'
    Note.where(id: @deps_full).map { |n| n.depends.all }.flatten.uniq - @deps_full
  end

  # will need fixing also
  def depends_deeper_pos
    Note.where(id: deps_full_pos).map(&:pos).flatten.uniq - @deps_full
  end

  def full_pos
    puts 'Depends.full_pos'
    deps_new = pos
    @deps_full = deps_new
    while deps_new.any?
      deps_new = depends_deeper_pos
      @deps_full += deps_new
      @deps_full = @deps_full.uniq
    end
    @deps_full
  end

  def self.depends_deeper_group
    Note.where(id: @deps_full).map { |n| n.depends.all }.flatten.uniq - @deps_full
  end

  def self.full_group(notes)
    deps_new = notes.map { |note| note.depends.all }.uniq
    @deps_full = deps_new
    while deps_new.any?
      deps_new = depends_deeper_group
      @deps_full += deps_new
      @deps_full = @deps_full.uniq
    end
    @deps_full
  end

  def full
    puts 'Depends.full'
    deps_new = all
    @deps_full = deps_new
    while deps_new.any?
      deps_new = depends_deeper
      @deps_full += deps_new
    end
    @deps_full
  end
end

# to be checked
# keeps a list for future checking, dnr.
class Tbc
  def self.list
    @tbc
  end

  def self.append(note_id)
    puts 'Tbc.append'
    Abc.append(note_id)
    @tbc ||= []
    @tbc.push(note_id)
    @tbc = @tbc.compact.flatten.uniq
    @tbc
  end

  def self.remove(note_id)
    @tbc -= [note_id]
  end

  def self.full_check
    @tbc ||= []
    cnt = 0
    while @tbc.any? || Abc.list.any?
      cnt += 1
      check_tbc(cnt) if @tbc.any?
      Abc.check unless @tbc.any?
    end
    puts @tbc.length
  end

  def self.check_tbc(cnt)
    puts 'check_tbc'
    ts = Note.tag_tags
    @tbc -= ts
    tbcs = Note.where(id: @tbc.sort)
    # tbcs.inject(tbcs) { |t_a, note| t_a + note.rev_magic_pos }.uniq
    tbcs = Note.rev_magic_group_pos(tbcs).uniq.sort + tbcs
    tbcs -= Note.find(ts)

    tbcs.each_with_index do |note, ind|
      next_tbc(note, cnt, tbcs.length - ind)
    end
    # return unless @tbc.empty?
    # puts @run_once.length.to_s.light_green
    # puts @run_twice.length.to_s.light_yellow
    # puts @run_thrice.length.to_s.yellow
    # puts @run_too_much.to_s.red
    # @run_too_much
  end

  def self.next_tbc(note, cnt, rmnd)
    puts 'next_tbc'
    [note.id].each do |n_id|
      Tbc.remove(n_id)
      Abc.append(n_id)
    end
    # Tbc.remove(note.id)
    # does this go here????
    # Abc.append(note.id)
    puts "\n\n********************* NEXT TBC ************************\n\n
    #{rmnd} #{cnt}\n\n".light_green
    note.refresh_magic if note.methods.include?(:refresh_magic)
    # deletions
  end

  def self.final_check
    puts 'final_check'
    Abc.check
  end
end

# already been checked
# recheck abc, mostly for remove olds
class Abc
  def self.list
    @abc
  end

  def self.append(tbc)
    @abc ||= []
    @abc += [tbc]
    @abc = @abc.uniq
  end

  def self.check
    return unless @abc.any?
    abc_l = Note.find(list)
    # abc_l.inject(abc_l) { |tbc, chk| tbc + chk.rev_magic }
    # .uniq.each(&:refresh_magic)
    abcs = abc_l + Note.rev_magic_group(abc_l)
    # abcs += Note.where(id: abc_l.map { |nt| nt.depends.full })
    abcs += Note.where(id: Depends.full_group(abc_l))
    abcs.uniq.sort.each_with_index do |note, ind|
      puts "ABC Check #{abcs.length - ind}: #{note.id} #{note.name}".light_green
      note.refresh_magic
    end
    @abc = []
  end
end

# a class for updating and checking the relatives of a note
class Rels
  def initialize(note)
    @rels = []
    @note = note
    rels_collection
    magic_filter
    @ids = @rels.map(&:child_id)
  end
  attr_reader :note, :rels

  def rels_collection
    magic_ands if @note[:and_ids].any?
    magic_ors
    magic_relatives
  end

  def current_ids
    @current_ids ||= note.child_relationships.reload.map(&:child_id)
  end

  def magic_relatives
    return unless @note[:relative_ids].any?
    rs = Relationship.distant_relatives(@note.relative_ids)
    # rs = Note.where(id: relative_ids).map(&:relatives).flatten
    # name needs to stay the same for future filtering
    rs = rs.map do |rel|
      Relationship.new(
        name: rel.name,
        parent_id: @note.id,
        child_id: rel.child_id
      )
    end
    @rels += rs
  end

  def magic_ors
    return unless @note[:or_ids].any?
    rs = @note.or_ids.inject([]) do |total, or_id|
      total + Note.find(or_id).relatives
    end
    @rels += rs.each { |rel| rel.parent_id = @note.id }.to_a
  end

  def magic_ands
    ns = Note.where(id: @note.and_ids).map(&:relatives)
    @rels += ns[1..-1].inject(ns[0].map(&:child_id)) { |ttl, note| ttl & note.map(&:child_id) }
                      .uniq
                      .map do |nt|
      Relationship.new(
        parent_id: @note.id,
        child_id: nt
      )
    end
  end

  def magic_filter
    # magic_filter_rname must be first
    magic_filter_rname
    # true_child_ids = (rels_b.map(&:child_id) - not_c_ids)
    #                  .select { |c_id| having_count(childs.count(c_id)) }
    # rels_c = rels_b.select { |r| true_child_ids.include?(r.child_id) }
    magic_filter_not_ids
    magic_filter_having_count
    magic_filter_tags
    puts 'magic_filter_done'
  end

  # has to be first magic_filter
  def magic_filter_rname
    puts 'magic_filter_rname'
    filter_rname = @note.filter_rname
    return unless @note.filter_rname.any?
    unless @note.depends.pos.any?
      puts 'puts no depends'
      @rels = Relationship.where(name: filter_rname)
      @rels += @rels.uniq(&:parent_id).map do |rel|
        puts 'is it here?' # looks like no
        Relationship.new(parent_id: @note.id, child_id: rel.parent_id)
      end
      @rels = @rels.each { |rel| rel.parent_id = @note.id }
      return @rels.length
    end
    puts 'with depends'
    @rels = @rels.select { |rel| filter_rname.include?(rel.name) }
    puts 'done with depends'
    @rels.length
  end

  # needs testing
  # was checking f_ids.include?(rel[:parent_id]) and flip
  # but cant figure why necessary?
  # rels should be in order bf they get here?
  def magic_filter_tags
    puts 'magic_filter_tags'
    filter_tags = @note.filter_tags
    return @rels.length unless filter_tags.any?
    f_ids = @rels.map(&:child_id).uniq
    f_ids = Note.where("ARRAY#{@note.filter_tags.to_s.tr('"', "'")} && tags")
                .where('id in (?)', f_ids)
                .select(:id)
                .map(&:id)
    @rels = @rels.select { |rel| f_ids.include?(rel[:child_id]) }
    @rels.length
    # new_rels += rels.select { |rel| f_ids.include?(rel[:parent_id]) }
    #                 .each(&:flip)

    # new_rels
  end

  def magic_filter_not_ids
    # ids = rels.map(&:child_id) - not_c_ids
    # ids.select { |c_id| having_count(childs.count(c_id)) }
    puts 'magic_filter_not_ids'

    bad_ids = not_c_ids
    @rels = @rels.reject { |rel| bad_ids.include?(rel.child_id) }
    @rels.length
  end

  def not_c_ids
    return [] unless @note.not_ids.any?
    not_c_ids = Relationship.where(parent_id: @note.not_ids).map(&:child_id)
    not_c_ids += Relationship.where(child_id: @note.not_ids).map(&:parent_id)
    not_c_ids
  end

  def magic_filter_having_count
    puts 'magic_filter_having_count'
    return @rels.length unless @note.having_count.present?
    rels_children = @rels.map(&:child_id)
    @rels = @rels.uniq.select do |rel|
      cnt = rels_children.count(rel.child_id)
      having_count(cnt)
    end
    puts 'done filter_having_count'
    @rels.length
  end

  def having_count(note_count)
    puts 'having_count'
    @op_cnt ||= @note.having_count.split(' ')
    note_count.send(@op_cnt[0], @op_cnt[1].to_i)
  end

  def update_relatives
    puts 'update_relatives'
    # keep "pre_rels" for debugging
    # pre_rels = rels

    # filter results
    # rels = magic_filter(rels)

    # find child_ids

    old_ids = current_ids - @ids

    puts 'remove old'
    @note.rebuild_relatives.each do |note_rel|
      note_rel.remove_old_child if old_ids.include?(note_rel.child_id)
    end

    # @rels.reject { |rel| @note.relatives.map(&:child_id).include?(rel.child_id) }
    puts 'new child'
    @rels.uniq {|rel| rel[:child_id]}.each do |rel|
      # c_id = rel.child_id
      # rel.remove_old_child if old_ids.include?(c_id)
      # unless current_ids.include?(rel.child_id)
      #   if Relationship.where(parent_id: rel.parent_id, child_id: rel.child_id).any?
      #     binding.pry_remote
      #   end
      # end
      rel.new_child unless current_ids.include?(rel.child_id)
    end

    # remove old children if not in childs
    # old_childs = child_relationships.where.not(child_id: childs)
    # old_childs = rels.select { |r| old_ids.include?(r.child_id) }
    # Relationship.remove_old_children(old_childs)

    # new_rs = rels.select { |rel| childs.include?(rel[:child_id]) }
    # childs -= child_relationships.map(&:child_id)
    # new_childs = rels.reject { |r| current_ids.include?(r.child_id) }
    # Relationship.new_childs(rels.select { |r| childs.include?(r[:child_id]) })
    # Relationship.new_childs(new_childs)
  end
end

# special testing methods
# i messed with this, i dont imagine it still works
# commented classes that reeked. they still work as much anything else here
class Special
  # def self.full_chain_test(n_id)
  #   Note.find(n_id).relatives.inject([]) do |abc, d_n|
  #     # puts 'skip' if @abc.include?(d_n.id)
  #     next abc if abc.include?(d_n.id)
  #     @a = d_n.test_chain
  #     # puts @d_n
  #     puts @a
  #     puts 'found' if @a.length.positive?
  #     break if @a.length.positive?
  #     abc + [d_n.id]
  #   end
  #   @abc
  # end

  # def test_chain
  #   ct = chain_talmidim
  #   cr = chain_rebbis
  #   puts ct.uniq.map(&:name)
  #   puts ct.uniq.map(&:name)
  #   puts ct & cr
  #   ct & cr
  # end

  # def chain_talmidim
  #   already_run = []
  #   ts = talmidim
  #   5.times do
  #     ts.each do |talmid|
  #       next if already_run.include?(talmid)
  #       ts += talmid.talmidim
  #       already_run.push(talmid)
  #     end
  #   end
  #   ts
  # end

  # def chain_rebbis
  #   already_run = []
  #   ts = rebbis
  #   5.times do
  #     ts.each do |talmid|
  #       next if already_run.include?(talmid)
  #       ts += talmid.rebbis
  #       already_run.push(talmid)
  #     end
  #   end
  #   ts
  # end

  # def kat(name)
  #   Note.find(relatives.select { |r| r.name == name }.map(&:child_id))
  #       .select { |r| r.tags.include?('כת תלמידים') }
  # end

  # def talmidim
  #   Note.find(
  #     kat('רב').map(&:relatives)
  #        .flatten
  #        .select { |r| r.name == 'תלמיד' }.map(&:child_id)
  #   ).select { |r| r.tags.include?('person') }
  # end

  # def rebbis
  #   Note.find(
  #     kat('תלמיד').map(&:relatives)
  #       .flatten.select { |r| r.name == 'רב' }.map(&:child_id)
  #   ).select { |r| r.tags.include?('person') }
  # end
end
