# frozen_string_literal: true

# configure rails_admin for the note model
module NoteAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
      configure :show_note do
        pretty_value do
          util = bindings[:object]
          %(<a href="/note/#{util.id}">#{util.id}</a>).html_safe
        end
        read_only true
        help ''
      end

      configure :add_birth_and_death do
        pretty_value do
          util = bindings[:object]
          %(<a href="/note/add_birth_and_death?id=#{util.id}">add birth and death</a>).html_safe
        end
        read_only true
        help ''
      end

      configure :name do
        searchable true
        queryable true
      end

      configure :text do
        partial 'note_text_partial.html.haml'
        help ''
      end

      configure :math_value do
        help 'to metres, litres,'
      end

      configure :parents do
        associated_collection_cache_all false # false is default if > 30
        associated_collection_scope do
          Proc.new { |scope|
            scope = scope.limit(100)
          }
        end
        help "don't forget to edit name in relationships"
      end


      configure :children do
        associated_collection_cache_all false # false is default if > 30
        associated_collection_scope do
          Proc.new { |scope|
            scope = scope.limit(100)
          }
        end
        help "don't forget to edit name in relationships"
      end

      list do
        fields :show_note, :name, :img, :ext_link_name, :summary, :updated_at
      end

      update do
        fields :show_note, :name, :is_private, :text, :img, :img320, :img_source,
               :ext_link, :ext_link_name, :int_link, :summary, :created_at,
               :magic, :math_value, :add_birth_and_death
        fields :locations, :children, :parents
        field :not_ids, :pg_string_array do
          help 'magic: not children of any. comma seperated, no spaces, autosaves!'
        end
        field :or_ids, :pg_string_array do
          help 'magic: children of any. comma seperated, no spaces, autosaves!'
        end
        field :and_ids, :pg_string_array do
          help 'magic: children of all. comma seperated, no spaces, autosaves!'
        end
        field :relative_ids, :pg_string_array do
          help 'magic: children of children of any. comma seperated, no spaces, autosaves!'
        end
        field :relative_ids_though_name do
          help 'relative_ids find distant relatives. this will limit the results to near relatives with this relationship name'
        end
        field :filter_tags, :pg_string_array do
          help "magic: children's tags must include .., comma separated, no spaces"
        end
        field :filter_rname, :pg_string_array do
          help "magic: relationship name must include .., comma separated, no spaces"
        end
        field :having_count do
          help "e.g. > 3"
        end
        field :tags, :pg_string_array do
          help 'comma seperated, no spaces'
        end
      end

      create do
        fields :name, :img, :img320, :img_source, :ext_link, :ext_link_name,
               :summary, :text, :is_private
        field :tags, :pg_string_array do
          help 'comma seperated, no spaces'
        end
      end
    end
  end
end


