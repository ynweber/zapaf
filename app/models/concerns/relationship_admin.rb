# frozen_string_literal: true

# configure rails_admin for the note model
module RelationshipAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
      configure :show_parent do
        pretty_value do
          util = bindings[:object]
          %(<a href="/note/#{util.parent_id}">#{util.parent_id}</a>).html_safe
        end
        read_only true
        help ''
      end

      configure :att_note do
        pretty_value do
          util = bindings[:object]
          %(<a href="/relationship/att_note?id=#{util.id}">attach note</a>).html_safe
        end
        read_only true
        help ''
      end

      create do
        fields :name, :parent, :show_parent, :child, :note_id, :att_note
      end

      update do
        fields :name, :parent, :show_parent, :child, :note, :att_note
      end
    end
  end
end
