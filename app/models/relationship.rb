# frozen_string_literal: true

# validator
class RelationshipValidator < ActiveModel::Validator
  def validate(rel)
    @rel = rel
    check_overlap
    check_parentischild
    rel = @rel
  end

  def check_overlap
    dups = []
    # prob if parent doesnt exist
    return unless @rel.parent
    dups += [@rel.child_id] & @rel.parent.parents.map(&:id)
    dups += [@rel.parent_id] & @rel.child.children.map(&:id)
    if dups.length.positive?
      err = 'parents overlap children'
      puts 'one of these:'.light_red
      puts @rel.child_id, @rel.parent_id
      @rel.errors.add :base, err
      puts err.light_red
    end
    # binding.pry_remote if dups.length.positive?
    # binding.pry_remote
  end

  def check_parentischild
    if @rel.parent_id == @rel.child_id
      err = "parent is child #{@rel.parent_id}" 
      @rel.errors.add :base, err
      puts err.light_red
    end
  end
end
# the reationship model
class Relationship < ApplicationRecord
  include RelationshipAdmin
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :validatable
  #
  # belongs_to :notes, class_name: 'Note', foreign_key: 'parent_id'
  belongs_to :note, class_name: 'Note', foreign_key: :note_id, autosave: false, optional: true
  belongs_to :parent,
             class_name: 'Note', foreign_key: :parent_id,
             autosave: false, optional: true
  belongs_to :child,
             class_name: 'Note', foreign_key: :child_id, autosave: false, optional: true
  delegate :name, :text, :summary, :img_small, :tags, :relatives, :is_private, to: :parent, prefix: true
  delegate :name, :text, :summary, :img_small, :tags, :relatives, :is_private, to: :child, prefix: true
  validates_with RelationshipValidator
  after_commit :for_tbc

  def for_tbc
    Tbc.append(child_id)
    Tbc.append(parent_id)
  end

  # once is the same as an or
  def self.distant_relatives(ids)
    return unless ids.any?
    ids_a = where(parent_id: ids)
    ids_b = where(child_id: ids).each {|rel| rel.child_id = rel.parent_id }
    ids_c = where(child_id: (ids_a + ids_b).map(&:child_id)).each { |rel| rel.child_id = rel.parent_id }
    ids_d = where(parent_id: (ids_a + ids_b).map(&:child_id))
    # ids_a = ids_a.where(name: r_name) if r_name.present?
    # ids_b = ids_b.where(name: r_name) if r_name.present?
    # ids_c = ids_a.map(&:child_id) + ids_b.map(&:parent_id)
    # rs = where(child_id: ids_a).each{ |rel| rel.child_id = rel.parent_id } # returns rel, child id = rav
    # rs += where(parent_id: ids_b)
    # rs
    
    ids_c + ids_d
    # # ids_a = where(parent_id: ids).map(&:child_id)
    # # ids_a += where(child_id: ids).map(&:parent_id)
    # # ids_a
    # ids_a = where(parent_id: ids).map(&:child_id)
    # ids_a += where(child_id: ids).map(&:parent_id)
    # # ids_a = where(parent_id: ids_a).map(&:child_id)
    # # ids_a += where(child_id: ids_a).map(&:parent_id)
    # rs = where(parent_id: ids_a) # .map(&:child_id)
    # rs += where(child_id: ids_a) # .map(&:parent_id)
    # # # not going to work with Relationship.new in flip !!!
    # # rs.reject { |c_id| ids_a.include?(c_id) }
  end

  def flip
    puts 'flip'
    old_c = self[:child_id]
    self[:child_id] = parent_id
    self[:parent_id] = old_c
    self
    #attributes(parent_id: child_id, child: parent_id)
    # Relationship.new(parent_id: child_id, child_id: parent_id)
  end

  def card_style
    return @r_styles if @r_styles
    r_styles = child_tags
    r_styles += ['r_card']
    unless child_tags.include?('tag')
      r_styles += child_relatives.map(&:child_name)
                                 .map { |r_name| r_name.tr(' ', '-').downcase }
    end
    @r_styles = r_styles
    @r_styles
  end

  # def self.related_to(note_id)
  #   relatives = where(parent_id: note_id).map do |c_note|
  #     note = c_note.child
  #     note.relationship = c_note
  #     note
  #   end
  #   relatives + where(child_id: note_id).map do |p_note|
  #     note = p_note.parent
  #     note.relationship = p_note
  #     note
  #   end
  # end
  #

  # def self.relatives_of(note_id)
  #   to_r = where(parent_id: note_id)
  #   to_r + where(child_id: note_id).map(&:flip)
  # end

  # def self.new_childs(new_rs)
  #   # remove from new rels, where already a child

  #   # !!!!! GET RID OF THIS
  #   # return new_rs.length

  #   # create new children
  #   new_rs.uniq.each do |rel|
  #     # I cant figure out why this is still necessary!!!
  #     # next if child_relationships.map(&:child_id).include?(rel.child_id)

  #     created_r = Relationship.find_or_create_by(
  #       parent_id: id, child_id: rel.child_id
  #     )
  #     created_r.name ||= rel.name || @magic_name
  #     created_r.save
  #     # Note.find(rel.child_id).rev_magic
  #     Note.tbc(rel.child_id)
  #   end
  #   new_rs.length || 0
  # end

  def new_child
    created_r = Relationship.where(
      parent_id: parent_id, child_id: child_id
    ).first_or_initialize
    created_r.name ||= name || @magic_name
    created_r.save
    created_r
  end

  def remove_old_child
    c_ids = [child_id, parent_id]
    delete
    puts c_ids.to_s.light_green
    c_ids.each { |old_c| Tbc.append(old_c) }
    c_ids
  end

  # def self.remove_old_children(old_rs)
  #   # old_rs = child_relationships.where.not(child_id: childs)
  #   old_cs = old_rs.map(&:child_id)
  #   old_cs += old_rs.map(&:parent_id)
  #   old_rs.each(&:delete)
  #   old_cs.each { |old_c| Note.tbc(old_c) }
  #   # recheck = old_cs.map do |c|
  #   #   # give a chance to clear junk before moving on
  #   #   rm = Note.find(c).rev_magic
  #   #   rm - Note.where('array[not_ids] && array[(?)]', [c])
  #   # end
  #   # recheck.flatten.uniq.each_with_index do |rec, ind|
  #   #   puts '---RECHECK---'.red
  #   #   puts ind.to_s.light_green + ' : ' + recheck.flatten.uniq.length.to_s.red
  #   #   rec.refresh_quick
  #   # end
  #   old_cs
  #   # .each(&:rev_magic)
  # end

  def sort_field(sf_name)
    return name if sf_name == 'rel_name'
    return if child.nil?
    child.send(sf_name)
  end
end
