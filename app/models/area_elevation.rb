class AreaElevation < ApplicationRecord
  has_many :locations
  belongs_to :unit, class_name: 'Note'

end
