class Location < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :validatable
  has_and_belongs_to_many :notes
  belongs_to :area_latitude
  belongs_to :area_longitude
  belongs_to :area_elevation
  belongs_to :area_datetime
  belongs_to :origin, -> { where("'origin' = ANY (tags)") }, class_name: 'Location'
  # belongs_to :origin, class_name: 'Location'

  rails_admin do

    configure :origin do
      associated_collection_scope do
        proc { |scope| scope.where("'origin' = ANY (tags)") }
      end
      help 'locations with tag "origin"'
    end

    list do
      fields :name, :origin
      field :tags, :pg_string_array
    end

    update do
      field :name
      field :origin do
        # associated_collection_scope { where("'origin' = ANY (tags)") }
      end
      field :area_latitude
      field :area_longitude
      field :area_elevation
      field :area_datetime
      field :tags, :pg_string_array do
        help 'comma separated, no spaces; tag "origin" as desired'
      end
      field :notes
    end
  end


  # !!! all of this was moved from model/note.rb
  # !!! will not work without adj

  # def self.by_location(search)
  #   children = {}
  #   children[:lat] = loc_lts(search[:latitude]) if search[:latitude]
  #   children[:lon] = loc_lgs(search[:longitude]) if search[:longitude]
  #   children[:ds] = loc_ds(search[:date]) if search[:date]
  #   children[:es] = loc_es(search[:elevation]) if search[:elevation]
  #   children[:ts] = loc_ts(search[:time]) if search[:time]
  #   children.values.reduce { |new_a, a| new_a & a }
  # end

  # test this!!
  def self.loc_lts(latitude)
    joins(locations: [:area_latitude])
      .where(
        "(? between area_latitudes.x and area_latitudes.y) or
        (area_latitudes.y + 90 between ? and area_latitudes.x)",
        latitude, latitude
      )
  end

  # made changes, will need retesting
  def self.loc_lgs(longitude)
    joins(locations: [:area_longitude])
      .where(
        "(area_longitudes.y > area_longitudes.x and
        ? between area_longitudes.x and area_longitudes.y) or
        ((area_longitudes.y < area_longitudes.x) and
        (? between area_longitudes.x and area_longitudes.y + 360))",
        longitude, longitude
      )
  end

  def self.loc_ds(date)
    joins(locations: [:area_datetime]).where(
      "'?'::date between area_datetimes.date_x and area_datetimes.date_y",
      Date.new(date.to_i)
    )
  end

  def self.loc_ts(time)
    joins(locations: [:area_datetime]).where(
      "'?'::time between area_datetimes.time_x and area_datetimes.time_y",
      Time.new(time.to_i)
    )
  end

  ### !!! end cut from model/note.rb

end
