class AreaDatetime < ApplicationRecord
  has_many :locations
  rails_admin do
    field :name
    field :date_x do
      # date_format :short
      strftime_format '%Y-%m-%d'
      partial 'date_partial.html.haml'
    end
    field :time_x
    field :date_y do
      strftime_format '%Y-%m-%d'
      partial 'date_partial.html.haml'
    end
    field :time_y
  end
end
