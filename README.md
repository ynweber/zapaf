# README

* Ruby 2.4
* Rails 5.2.1

This is a work in development.

Zapaf is a note taking app.
I kept it simple: title, summary, text. Also a picture and an external link. Locations can be attached, describing a place and time. (A map will be created)

The notes are meant to be kept simple.
If it requires two pictures or two links, use two notes.

Where things get interesting are the relationships.
Each note can relate to another, in a hiarchy if you like, or in a webbed mess if you prefer.
For example. One note can describe Presidents of the United States. Another note could describe George Washington. Then they can be given a relationship 'First' or 'One' or 'Best' or 'Worst' or whatever.
Another note can be attached to the relationship to further describe the relaitionship if desired.

Then things get really interesting with 'Magic' notes.
A magic note creates its own relationships. e.g. A note 'National Leaders' could have a magic 'or' note list referencing 'Presidents of the United States', 'Prime Ministers of England', whatever. 
Another note could be '18th Century National Leaders' with an or list of just 'National Leaders' and a filter for '18th Century'.

As stated, this is a work in development, many features undeveloped and/or just an idea. However I myself use this regularly and current have 68180 notes 110790 relationships.
