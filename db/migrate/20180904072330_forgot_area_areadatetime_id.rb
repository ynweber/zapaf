class ForgotAreaAreadatetimeId < ActiveRecord::Migration[5.2]
  def change
    rename_column :areas, :areadatetime_id, :area_datetime_id
  end
end
