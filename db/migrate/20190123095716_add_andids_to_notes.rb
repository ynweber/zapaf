class AddAndidsToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :and_ids, :int, default: [], array: true
  end
end
