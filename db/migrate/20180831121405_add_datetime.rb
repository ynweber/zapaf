class AddDatetime < ActiveRecord::Migration[5.2]
  def change
    remove_column :areas, :tim_x
    remove_column :areas, :tim_y
    add_column :areas, :date_x, :datetime
    add_column :areas, :date_y, :datetime
  end
end
