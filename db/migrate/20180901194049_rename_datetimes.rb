class RenameDatetimes < ActiveRecord::Migration[5.2]
  def change
    rename_table :area_datetimes, :datetimes
  end
end
