class UpdateAreas < ActiveRecord::Migration[5.2]
  def change
    remove_column :areas, :lat_x
    remove_column :areas, :lat_y
    remove_column :areas, :lon_y
    remove_column :areas, :lon_x
    remove_column :areas, :alt_x
    remove_column :areas, :alt_y
    remove_column :areas, :date_x
    remove_column :areas, :date_y
    add_column :areas, :area_latitude_id, :integer
    add_column :areas, :area_longitude_id, :integer
    add_column :areas, :area_elevation_meter_id, :integer
    add_column :areas, :area_datetime_id, :integer
  end
end
