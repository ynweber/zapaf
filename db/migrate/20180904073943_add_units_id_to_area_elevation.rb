class AddUnitsIdToAreaElevation < ActiveRecord::Migration[5.2]
  def change
    rename_table :area_elevation_meters, :area_elevations
    add_column :area_elevations, :unit_id, :bigint
  end
end
