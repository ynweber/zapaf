class CreateNoteIdOnRelationships < ActiveRecord::Migration[5.2]
  def change
    add_column :relationships, :note_id, :string
    add_column :locations, :note_id, :string
    add_column :notes, :img_source, :string
  end
end
