class AddExtLinkNameToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :ext_link_name, :string
  end
end
