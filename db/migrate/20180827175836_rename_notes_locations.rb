class RenameNotesLocations < ActiveRecord::Migration[5.2]
  def change
    rename_column :notes_locations, :locations_id, :location_id
    rename_column :notes_locations, :notes_id, :note_id
  end
end
