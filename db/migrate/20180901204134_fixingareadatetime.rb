class Fixingareadatetime < ActiveRecord::Migration[5.2]
  def change
    rename_column :areas, :datetime_id, :areadatetime_id
  end
end
