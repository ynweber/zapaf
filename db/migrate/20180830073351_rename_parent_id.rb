class RenameParentId < ActiveRecord::Migration[5.2]
  def change
    rename_column :notes, :parent_id, :prime_id
    rename_column :relationships, :note_id, :parent_id
  end
end
