class RemoveColTagsPrimeId < ActiveRecord::Migration[5.2]
  def change
    remove_column :notes, :prime_id
    remove_column :notes, :tags
    remove_column :relationships, :tags
    remove_column :locations, :tags
  end
end
