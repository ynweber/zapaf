class RenameTableLocationsNotes < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :locations_notes, :notes_locations
  end

  def self.up
    rename_table :notes_locations, :locations_notes
  end
end
