class RenameArea < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :areas, :area
  end

  def self.up
    rename_table :area, :areas
  end
end
