class RenameDatetime < ActiveRecord::Migration[5.2]
  def change
    rename_table :area_datetime, :area_datetimes
  end
end
