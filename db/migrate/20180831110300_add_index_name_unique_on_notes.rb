class AddIndexNameUniqueOnNotes < ActiveRecord::Migration[5.2]
  def change
    add_index :notes, :name, unique: true
  end
end
