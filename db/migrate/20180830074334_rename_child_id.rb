class RenameChildId < ActiveRecord::Migration[5.2]
  def change
    rename_column :relationships, :child_note, :child_id

  end
end
