class DropAndRecreateDatetimes < ActiveRecord::Migration[5.2]
  def change
    drop_table :datetimes
    create_table :areadatetimes do |t|
      t.string :name
      t.datetime :x
      t.datetime :y
      t.timestamps
    end
  end
end
