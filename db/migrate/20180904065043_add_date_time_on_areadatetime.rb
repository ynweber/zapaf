class AddDateTimeOnAreadatetime < ActiveRecord::Migration[5.2]
  def change
    rename_table :areadatetimes, :area_datetimes
    add_column :areas, :date_x, :date, default: Date.new(-3761, 9, 9)
    add_column :areas, :time_x, :time, default: Time.new(0, 1, 1, 0, 0, 0)
    add_column :areas, :date_y, :date, default: Date.new(2239, 9, 30)
    add_column :areas, :time_y, :time, default: Time.new(0, 1, 1, 23, 59, 59)
  end
end
