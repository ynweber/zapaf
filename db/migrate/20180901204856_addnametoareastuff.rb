class Addnametoareastuff < ActiveRecord::Migration[5.2]
  def change
    add_column :area_latitudes, :name, :string
    add_column :area_longitudes, :name, :string
    add_column :area_elevation_meters, :name, :string
  end
end
