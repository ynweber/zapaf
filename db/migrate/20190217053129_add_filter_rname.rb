class AddFilterRname < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :filter_rname, :string, default: [], array: true
  end
end
