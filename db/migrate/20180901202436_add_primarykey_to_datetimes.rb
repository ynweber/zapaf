class AddPrimarykeyToDatetimes < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :areas, :datetimes
    remove_column :datetimes, :id
    add_column :datetimes, :id, :primary_key
  end
end
