class CreateNotesAndLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :notes do |t|
      t.string :name
      t.string :f_name
      t.text :tags, array: true, default: []
      t.timestamps
    end

    create_table :notes_notes, id: false do |t|
      t.belongs_to :note, index: true
      t.references :child_note, index: true
    end

    create_table :locations do |t|
      t.string :name
      t.text :tags, array: true, default: []
      t.timestamps
    end

    create_table :notes_locations, id: false do |t|
      t.belongs_to :notes, index: true
      t.belongs_to :locations, index: true
    end

    create_table :area do |t|
      t.string :name
      t.float :lat_x, default: -90
      t.float :lat_y, default: 90
      t.float :lon_x, default: -180
      t.float :lon_y, default: 180
      t.float :alt_x, default: -500
      t.float :alt_y, default: 10_000
      t.float :tim_x, default: 0
      t.float :tim_y, default: 10_000
      t.text :tags, array: true, default: []
      t.timestamps
    end

    create_table :locations_areas, id: false do |t|
      t.belongs_to :locations, index: true
      t.belongs_to :area, index: true
    end
  end
end
