class AddExtLink < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :ext_link, :string
  end
end
