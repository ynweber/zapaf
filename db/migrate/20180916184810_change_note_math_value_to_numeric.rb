class ChangeNoteMathValueToNumeric < ActiveRecord::Migration[5.2]
  def change
    change_column :notes, :math_value, :numeric
  end
end
