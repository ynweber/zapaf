class FixDatetime < ActiveRecord::Migration[5.2]
  def change
    drop_table :datetimes
    create_table :datetimes do |t|
      t.string :name
      t.datetime :x, default: DateTime.new(-3761,9,9)
      t.datetime :y, default: DateTime.new(2239,9,30)
      t.timestamps
    end
  end
end
