class RenameLocationsAreas < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :areas_locations, :locations_areas
  end

  def self.up
    rename_table :locations_areas, :areas_locations
  end
end
