class IsPrivateDefault < ActiveRecord::Migration[5.2]
  def change
    change_column :notes, :is_private, :boolean, default: false
  end
end
