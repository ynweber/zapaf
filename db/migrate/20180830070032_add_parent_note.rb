class AddParentNote < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :parent_id, :string
  end
end
