class CreateAreas < ActiveRecord::Migration[5.2]
  def change
    create_table :area_latitudes do |t|
      t.float :x, default: -90
      t.float :y, default: 90
    end
    create_table :area_longitudes do |t|
      t.float :x, default: -180
      t.float :y, default: 180
    end
    create_table :area_elevation_meters do |t|
      t.float :x, default: -11_000
      t.float :y, default: 10_000
    end
    create_table :area_datetime do |t|
      t.datetime :x, default: DateTime.new(-3761,9,9)
      t.float :y, default: DateTime.new(2239,9,30)
    end
  end
end
