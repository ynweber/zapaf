class ChangeTimeDefaultToNil < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:area_datetimes, :time_x, nil)
    change_column_default(:area_datetimes, :time_y, nil)
  end
end
