class AddIndexTagsOnNotes < ActiveRecord::Migration[5.2]
  def change
    add_index :notes, :tags
  end
end
