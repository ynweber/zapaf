class ChangeMagicJson < ActiveRecord::Migration[5.2]
  def change
    remove_column :notes, :magic
    add_column :notes, :magic, :jsonb
  end
end
