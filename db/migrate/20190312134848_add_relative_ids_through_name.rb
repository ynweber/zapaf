class AddRelativeIdsThroughName < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :relative_ids_though_name, :string
  end
end
