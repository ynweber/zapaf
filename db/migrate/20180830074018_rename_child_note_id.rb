class RenameChildNoteId < ActiveRecord::Migration[5.2]
  def change
    rename_column :relationships, :child_note_id, :child_note

  end
end
