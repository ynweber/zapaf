class AddOriginToAreas < ActiveRecord::Migration[5.2]
  def change
    add_column :areas, :origin_id, :bigint
  end
end
