class AddIsprivateTagsToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :is_private?, :boolean
    add_column :notes, :tags, :text, default: [], array: true
  end
end
