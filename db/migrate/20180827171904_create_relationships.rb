class CreateRelationships < ActiveRecord::Migration[5.2]
  def change
    drop_table :notes_notes
    create_table :relationships do |t|
      t.text :name
      t.references :note, index: true
      t.references :child_note, index: true
      t.text :tags, array: true, default: []
    end
  end
end
