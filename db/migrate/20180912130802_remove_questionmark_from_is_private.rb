class RemoveQuestionmarkFromIsPrivate < ActiveRecord::Migration[5.2]
  def change
    rename_column :notes, :is_private?, :is_private
  end
end
