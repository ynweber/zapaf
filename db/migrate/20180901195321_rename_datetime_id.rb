class RenameDatetimeId < ActiveRecord::Migration[5.2]
  def change
    remove_column :areas, :area_datetime_id
    add_reference :areas, :datetime, foreign_key: true, index: true
  end
end
