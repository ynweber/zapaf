class AddUniqIndexParentChildRelationship < ActiveRecord::Migration[5.2]
  def change
    add_index :relationships, [:parent_id, :child_id], unique: true
  end
end
