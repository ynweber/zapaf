class AddNotIds < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :not_ids, :int, default: [], array: true
  end
end
