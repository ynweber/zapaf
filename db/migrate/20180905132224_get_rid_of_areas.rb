class GetRidOfAreas < ActiveRecord::Migration[5.2]
  def change
    drop_table :areas_locations
    drop_table :locations
    rename_table :areas, :locations
    add_column :area_datetimes, :note_id, :bigint
    add_column :area_elevations, :note_id, :bigint
    add_column :area_latitudes, :note_id, :bigint
    add_column :area_longitudes, :note_id, :bigint
    add_column :notes, :math_value, :bigint
    add_column :notes, :magic, :string
  end
end
