class AddSummaryText < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :summary, :string
    add_column :notes, :text, :string
  end
end
