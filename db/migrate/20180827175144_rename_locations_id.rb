class RenameLocationsId < ActiveRecord::Migration[5.2]
  def change
    rename_column :areas_locations, :locations_id, :location_id
  end
end
