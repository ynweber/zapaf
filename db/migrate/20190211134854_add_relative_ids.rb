class AddRelativeIds < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :relative_ids, :int, default: [], array: true
    add_column :notes, :filter_tags, :text, default: [], array: true
  end
end
