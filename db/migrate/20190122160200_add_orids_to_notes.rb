class AddOridsToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :or_ids, :int, default: [], array: true
  end
end
