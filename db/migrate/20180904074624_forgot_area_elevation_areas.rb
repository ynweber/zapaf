class ForgotAreaElevationAreas < ActiveRecord::Migration[5.2]
  def change
    rename_column :areas, :area_elevation_meter_id, :area_elevation_id
  end
end
