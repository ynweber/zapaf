class AddInternalLink < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :int_link, :string
  end
end
