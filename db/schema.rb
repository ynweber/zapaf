# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_17_094843) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "area_datetimes", force: :cascade do |t|
    t.string "name"
    t.datetime "x"
    t.datetime "y"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "date_x", default: "-3761-09-09"
    t.time "time_x"
    t.date "date_y", default: "2239-09-30"
    t.time "time_y"
    t.bigint "note_id"
  end

  create_table "area_elevations", force: :cascade do |t|
    t.float "x", default: -11000.0
    t.float "y", default: 10000.0
    t.string "name"
    t.bigint "unit_id"
    t.bigint "note_id"
  end

  create_table "area_latitudes", force: :cascade do |t|
    t.float "x", default: -90.0
    t.float "y", default: 90.0
    t.string "name"
    t.bigint "note_id"
  end

  create_table "area_longitudes", force: :cascade do |t|
    t.float "x", default: -180.0
    t.float "y", default: 180.0
    t.string "name"
    t.bigint "note_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "name"
    t.text "tags", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "area_latitude_id"
    t.integer "area_longitude_id"
    t.integer "area_elevation_id"
    t.bigint "area_datetime_id"
    t.bigint "origin_id"
    t.index ["area_datetime_id"], name: "index_locations_on_area_datetime_id"
  end

  create_table "locations_notes", id: false, force: :cascade do |t|
    t.bigint "note_id"
    t.bigint "location_id"
    t.index ["location_id"], name: "index_locations_notes_on_location_id"
    t.index ["note_id"], name: "index_locations_notes_on_note_id"
  end

  create_table "notes", force: :cascade do |t|
    t.string "name"
    t.string "img"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ext_link"
    t.string "summary"
    t.string "text"
    t.string "img_source"
    t.string "img320"
    t.string "ext_link_name"
    t.decimal "math_value"
    t.boolean "is_private", default: false
    t.text "tags", default: [], array: true
    t.jsonb "magic"
    t.integer "or_ids", default: [], array: true
    t.integer "and_ids", default: [], array: true
    t.integer "relative_ids", default: [], array: true
    t.text "filter_tags", default: [], array: true
    t.text "having_count"
    t.integer "not_ids", default: [], array: true
    t.string "filter_rname", default: [], array: true
    t.string "int_link"
    t.string "relative_ids_though_name"
    t.index ["and_ids", "or_ids", "not_ids", "relative_ids", "filter_tags"], name: "index_notes_on_and_ids_and_or_ids_and_relative_ids_and_not_ids_"
    t.index ["and_ids", "or_ids"], name: "index_notes_on_and_ids_and_or_id"
    t.index ["and_ids"], name: "index_notes_on_and_ids"
    t.index ["name"], name: "index_notes_on_name", unique: true
    t.index ["or_ids"], name: "index_notes_on_or_ids"
    t.index ["tags"], name: "index_notes_on_tags"
  end

  create_table "relationships", force: :cascade do |t|
    t.text "name"
    t.bigint "parent_id"
    t.bigint "child_id"
    t.string "note_id"
    t.index ["child_id"], name: "relationships_child_id"
    t.index ["parent_id"], name: "relationships_parent_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
