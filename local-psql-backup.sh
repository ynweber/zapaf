psql zapaf-ug_development -c '\COPY notes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/notes.csv
psql zapaf-ug_development -c '\COPY locations TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/locations.csv
psql zapaf-ug_development -c '\COPY relationships TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/relationships.csv
psql zapaf-ug_development -c '\COPY locations_notes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/locations_notes.csv
psql zapaf-ug_development -c '\COPY area_datetimes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/area_datetimes.csv
psql zapaf-ug_development -c '\COPY area_elevations TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/area_elevations.csv
psql zapaf-ug_development -c '\COPY area_latitudes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/area_latitudes.csv
psql zapaf-ug_development -c '\COPY area_longitudes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > ~/stuff/zapaf-ug-stuff/db_backup/area_longitudes.csv
