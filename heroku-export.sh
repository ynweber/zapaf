psql zapaf-ug_development -c '\COPY (select * from notes where char_length(text) > 140) TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > export/notes.csv
psql zapaf-ug_development -c '\COPY locations TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > export/locations.csv
psql zapaf-ug_development -c '\COPY relationships TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > export/relationships.csv
psql zapaf-ug_development -c '\COPY locations_notes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > export/locations_notes.csv
psql zapaf-ug_development -c '\COPY area_datetimes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > export/area_datetimes.csv
psql zapaf-ug_development -c '\COPY area_elevations TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > export/area_elevations.csv
psql zapaf-ug_development -c '\COPY area_latitudes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > export/area_latitudes.csv
psql zapaf-ug_development -c '\COPY area_longitudes TO STDOUT WITH (FORMAT csv, DELIMITER ",", HEADER true)' > export/area_longitudes.csv
