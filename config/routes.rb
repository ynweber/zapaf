# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  devise_for :relationships
  devise_for :locations
  devise_for :application_records
  devise_for :notes
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root to: 'notes#show'
  get '/testing' => 'testing#testing', as: :testing
  get '/note/exec_int/' => 'notes#exec_int', as: :exec_int
  get '/note/add_birth_and_death' => 'notes#add_birth_and_death', as: :add_b_d
  get '/note/chain' => 'notes#chain', as: :chain
  get '/note/refresh' => 'notes#refresh', as: :refresh
  get '/note/refresh_quick' => 'notes#refresh_quick', as: :refresh_quick
  get '/note/read/:id' => 'notes#read', as: :read, :defaults => { id: '66' }
  get '/note/:id' => 'notes#show', as: :notes, :defaults => { id: '66' }
  get '/note/testing' => 'notes#testing', as: :notes_testing
  get '/note/:id' => 'notes#show', as: :note
  get '/locations/search' => 'locations#search', as: :locations_search
  get '/locations/search_results' => 'locations#show', as: :locations
  get '/special/spreadsheet/import' => 'special#spreadsheet_import'
  get '/special/spreadsheet/warning' => 'special#spreadsheet_warning', as: :siw
  get '/relationship/att_note' => 'relationships#att_note', as: :att_rel_note
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
